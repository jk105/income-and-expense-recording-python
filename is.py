'''
Income & Expense Tracking Program
By: jk105
'''

# For commandline argument
import sys
# For checking file existance
import pathlib
# For adding column in txt file
import numpy
# For default month and day input
from datetime import date


# Income & Expense List
income_list  = ["salary", "others"]
expense_list = ["food", "rent", "internet", "utility", "others"]


# Print help message
def print_help():
    print("Usage:\n -i: sum income\n -e: sum expense\n -n: sum net income")
    return


def main():
    try:
        # If 1 argument is provided
        if (len(sys.argv) == 2):
            path = pathlib.Path('spending.txt')
            # Calculate total income
            if (sys.argv[1] == "-i"):
                if path.exists():
                    data = numpy.loadtxt('spending.txt', delimiter=',', usecols=4)
                    income_data = data[data > 0]
                    total_income = numpy.sum(income_data)
                    print("Total Income: %.2f" % (total_income))
                else:
                    print("file doesn't exist")
            # Calculate total expense
            elif (sys.argv[1] == "-e"):
                if path.exists():
                    data = numpy.loadtxt('spending.txt', delimiter=',', usecols=4)
                    expense_data = data[data < 0]
                    total_expense = numpy.sum(expense_data)
                    print("Total Expense: %.2f" % (total_expense))
                else:
                    print("file doesn't exist")
            # Calculate net income
            elif (sys.argv[1] == "-n"):
                if path.exists():
                    data = numpy.loadtxt('spending.txt', delimiter=',', usecols=4)
                    net_income = numpy.sum(data)
                    print("Net Income: %.2f" % (net_income))
                else:
                    print("file doesn't exist")
            else:
                print_help()
                    
        # If no argument was provided
        elif (len(sys.argv) == 1):
            # Month
            while True:
                # Get user input
                print("Month:\n> ", end='')
                month_raw = input()

                # Try will catch error when using int(month) with non-integer values and continue the loop
                try:
                    month = int(month_raw)
                    # Stop loop if month is between 1 ~ 12
                    if (month >= 1) and (month <= 12):
                        break
                except ValueError:
                    # Stop loop if blank input and print current month
                    if not month_raw:
                        month = date.today().month
                        print(month)
                        break
                    continue 

            # Day
            while True:
                # Get user input
                print("Day:\n> ", end='')
                day_raw = input()

                try:
                    day = int(day_raw)
                    # For months April, June, Sept, Nov, stop loop if day is between 1 ~ 30
                    if (month == 4 or month == 6 or month == 9 or month == 11):
                        if (day >= 1) and (day <= 30):
                            break
                    # February
                    elif month == 2:
                        if (day >= 1) and (day <= 29):
                            break
                    else:
                        if (day >= 1) and (day <= 31):
                            break
                except ValueError:
                    if not day_raw:
                        day = date.today().day
                        print(day)
                        break
                    continue

            # Income or Expense
            while True:
                # Get user input
                print("Income(i) or Expense(e):\n> ", end='')
                iore = input()
                
                # Only accept 'i' or 'e'
                if (iore == 'i'):
                    iore = "income"
                    break
                elif (iore == 'e'):
                    iore = "expense"
                    break

            # Item
            while True:
                # Get user input
                print("Item:\n> ", end='')
                item = input()
                
                # Reject empty entry
                if (len(item) > 0):
                    break

            # Category
            while True:
                print("Category:\n", end='')
                if iore == 'income':
                    # List income category
                    for i in range(0, len(income_list)):
                        print("(%d) %s" % (i, income_list[i]))
                elif iore == 'expense':
                    # List expense category
                    for e in range(0, len(expense_list)):
                        print("(%d) %s" % (e, expense_list[e]))

                try:
                    # Get user input
                    print("> ", end='')
                    category = int(input())
                    # Only accept the listed options
                    if (category < len(income_list)) and (category >= 0) and (iore == 'income'):
                        category = income_list[category]
                        break
                    elif (category < len(expense_list)) and (category >= 0) and (iore == 'expense'):
                        category = expense_list[category]
                        break
                except ValueError:
                    continue

            # Amount
            while True:
                try:
                    # Get user input
                    print("Amount:\n> ", end='')
                    amount = float(input())
                    if iore == "income":
                        # Income should always be a positive number
                        amount = abs(amount)
                    if iore == "expense":
                        # Expense a negative number
                        amount = abs(amount) * -1
                    break
                except ValueError:
                    continue

            # Save entries to a txt file - spending.txt
            spending = open("spending.txt", "a+")
            print("Saved: %d/%d, %s, %s, %s, %.2f" % (month, day, iore, item, category, amount))

            # Write to file
            spending.write("%d/%d,%s,%s,%s,%.2f\n" % (month, day, iore, item, category, amount))

            # Close file
            spending.close()

        else:
            print_help()
        
        return

    except KeyboardInterrupt:
        sys.exit("\nExiting Program...")


if __name__ == "__main__":
    main()
