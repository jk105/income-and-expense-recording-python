Prerequisite<br/>
`$ pip install numpy`<br/><br/>

Simply run the code by executing<br/>
`$ python is.py`<br/>
![screenshot1](/uploads/67aae5da43a6357722cc82cf04d1c52c/screenshot1.png)<br/><br/>

A text file `spending.txt` will be saved on the same directory that you ran the code.<br/>
![screenshot2](/uploads/4f1beb2a955acf6a963782d74f779b0f/screenshot2.png)<br/><br/>

Calculate total income (`-i`), total expense (`-e`) & net income (`-n`)<br/>
![screenshot3](/uploads/aa48deddf55c9c84e051a7c30fc68208/screenshot3.png)<br/>
![screenshot4](/uploads/959cfdf9b7412141f33b07c9dc600470/screenshot4.png)<br/>
![screenshot5](/uploads/cda75effa5ecfaf8da943b9add1805c6/screenshot5.png)<br/>
